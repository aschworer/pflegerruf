package roomservice;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

import java.util.HashSet;
import java.util.Set;

/**
 * The point of entry for Lambda
 */
public class AlexaHandler extends SpeechletRequestStreamHandler {
    private static final Set<String> supportedApplicationIds = new HashSet<>();

    static {
        /*
         * This Id can be found on https://developer.amazon.com/edw/home.html#/ "Edit" the relevant
         * Alexa Skill and put the relevant Application Ids in this Set.
         */
        supportedApplicationIds.add("amzn1.ask.skill.6ab06e93-424f-464c-a5ca-d50bba289290");//main skill
        supportedApplicationIds.add("amzn1.ask.skill.d69656aa-f296-4b45-82c7-8d8e3817ab0e");//backup skill
    }

    public AlexaHandler() {
        super(new RoomServiceSpeechlet(), supportedApplicationIds);
    }
}
