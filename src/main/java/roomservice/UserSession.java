package roomservice;

import com.amazon.speech.speechlet.Session;

/**
 * Data storage to remember user speech history (to handle "incorrect" requests etc.)
 */
public class UserSession {

    public static final String LAST_SPOKEN_CARD = "lastSpokenCard";
    public static final String LAST_SPOKEN_SPEECH = "lastSpokenSpeech";
    private Session session;

    public UserSession(Session session) {
        this.session = session;
    }

    public String getLastSpokenCard() {
        return (String) session.getAttribute(LAST_SPOKEN_CARD);
    }

    public void setLastSpokenCard(String lastSpokenCard) {
        session.setAttribute(LAST_SPOKEN_CARD, lastSpokenCard);
    }

    public String getLastSpokenSpeech() {
        return (String) session.getAttribute(LAST_SPOKEN_SPEECH);
    }

    public void setLastSpokenSpeech(String lastSpokenSpeech) {
        session.setAttribute(LAST_SPOKEN_SPEECH, lastSpokenSpeech);
    }

    public String getReason() {
        return (String) session.getAttribute("reason");
    }

    public void setReason(String reason) {
        session.setAttribute("reason", reason);
    }

    public String getMealChoiceTimeOfTheDay() {
        return (String) session.getAttribute("mealOfTheDay");
    }

    public void setMealChoiceTimeOfTheDay(String mealOfTheDay) {
        session.setAttribute("mealOfTheDay", mealOfTheDay);
    }

    public String getMealChoiceNo() {
        return (String) session.getAttribute("no");
    }

    public void setMealChoiceNo(String no) {
        session.setAttribute("no", no);
    }

    public void setFlow(String flow) {
        session.setAttribute("flow", flow);
    }

    public boolean isOverrideMealFlow() {
        return "OverrideMeal".equalsIgnoreCase((String) session.getAttribute("flow"));
    }

    public boolean askedFor(String lastAlexaCard) {
        return lastAlexaCard.equals(session.getAttribute(LAST_SPOKEN_CARD));
    }

    public void setUserName(String name) {
        session.setAttribute("name", name);
    }

    public void setUserGender(String gender) {
        session.setAttribute("gender", gender);
    }

    public void setUserRoomNumber(String room) {
        session.setAttribute("room", room);
    }

    public String getGender() {
        return (String) session.getAttribute("gender");
    }

    public String getPatientName() {
        return (String) session.getAttribute("name");

    }

    public void setUserZip(String zip) {
        session.setAttribute("zip", zip);

    }

    public void setUserCity(String city) {
        session.setAttribute("city", city);

    }

    public String getCity() {
        return (String) session.getAttribute("city");

    }

    public String getRoom() {
        return (String) session.getAttribute("room");

    }

    public String getStreet() {
        return (String) session.getAttribute("street");

    }

    public void setStreet(String street) {
        session.setAttribute("street", street);
    }

    public String getZip() {
        return (String) session.getAttribute("zip");

    }
}
