package roomservice;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The main "shell" class to handle Alexa request-response
 */
public class RoomServiceSpeechlet implements SpeechletV2 {

    private static final Logger log = LoggerFactory.getLogger(RoomServiceSpeechlet.class);
    private Speaker speaker = new Speaker();
    private Responder responder;
    private PatientRepository repository = new PatientRepository();

    public RoomServiceSpeechlet() {
        responder = new Responder();
    }

    @Override
    public void onSessionStarted(SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
        log.info("onSessionStarted locale={} requestId={}, sessionId={}, deviceId={}",
                requestEnvelope.getRequest().getLocale(), requestEnvelope.getRequest().getRequestId(), requestEnvelope.getSession().getSessionId(),
                requestEnvelope.getSession().getUser().getUserId());
    }

    @Override
    public SpeechletResponse onLaunch(SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {
        log.info("onLaunch locale={} requestId={}, sessionId={}, deviceId={}",
                requestEnvelope.getRequest().getLocale(), requestEnvelope.getRequest().getRequestId(), requestEnvelope.getSession().getSessionId(),
                requestEnvelope.getSession().getUser().getUserId());
        UserSession session = new UserSession(requestEnvelope.getSession());
        Patient patient = repository.getPatient(requestEnvelope.getSession().getUser().getUserId());
        session.setUserName(patient.getName());
        session.setUserGender(patient.getGender());
        session.setUserRoomNumber(patient.getRoom());
        session.setStreet(patient.getStreet());
        session.setUserZip(patient.getZip());
        session.setUserCity(patient.getCity());
        return speaker.repeatedSpeechWelcome(session);
    }

    @Override
    public SpeechletResponse onIntent(SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {
        Intent intent = requestEnvelope.getRequest().getIntent();
        log.info("onIntent locale={} requestId={}, sessionId={}, deviceId={}", requestEnvelope.getRequest().getLocale(),
                requestEnvelope.getRequest().getRequestId(), requestEnvelope.getSession().getSessionId(),
                requestEnvelope.getSession().getUser().getUserId());
        UserSession sessionDetails = new UserSession(requestEnvelope.getSession());
        log.info("Session on request - " + sessionDetails);
        try {
            SpeechletResponse speechletResponse = responder.respondToIntent(requestEnvelope, intent, sessionDetails);
            log.info("Session on response - " + sessionDetails);
            return speechletResponse;
        } catch (Exception e) {
            log.error("Exception: ", e);
            throw e;
        }
    }

    @Override
    public void onSessionEnded(SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {
        log.info("onSessionEnded locale={} requestId={}, sessionId={}, deviceId={}",
                requestEnvelope.getRequest().getLocale(), requestEnvelope.getRequest().getRequestId(), requestEnvelope.getSession().getSessionId(),
                requestEnvelope.getSession().getUser().getUserId());
    }

}
