package roomservice;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Meal DynamoDb entity
 */
@DynamoDBTable(tableName = "Pflegerruf-Meals")
@NoArgsConstructor
public class Meal {

    @DynamoDBHashKey(attributeName = "id")
    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "timeOfTheDay")
    private String timeOfTheDay;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "no")
    private String no;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "meal")
    private String meal;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "day")
    private String day;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "date")
    private String date;

    public Meal(String timeOfTheDay, String no, String meal, String day, String date) {
        this.timeOfTheDay = timeOfTheDay;
        this.no = no;
        this.meal = meal;
        this.day = day;
        this.date = date;
    }

    //hack for Alexa bug with Synonyms in German
    static String resolveMeal(String meal) {
        if ("Frühstück".equalsIgnoreCase(meal) || "Frühstücksessen".equalsIgnoreCase(meal)) {
            return "Frühstück";
        } else if ("Mittag".equalsIgnoreCase(meal) || "Mittagessen".equalsIgnoreCase(meal)) {
            return "Mittagessen";
        } else if ("Abend".equalsIgnoreCase(meal) || "Abendessen".equalsIgnoreCase(meal)) {
            return "Abendessen";
        }
        return null;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id='" + id + '\'' +
                ", timeOfTheDay='" + timeOfTheDay + '\'' +
                ", no='" + no + '\'' +
                ", meal='" + meal + '\'' +
                ", day='" + day + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
