package roomservice;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Patient DynamoDB entity
 */
@DynamoDBTable(tableName = "Pflegerruf-Patients")
@NoArgsConstructor
public class Patient {
    @DynamoDBHashKey(attributeName = "id")
    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "deviceId")
    private String deviceId;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "room")
    private String room;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "gender")
    private String gender;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "name")
    private String name;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "street")
    private String street;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "zip")
    private String zip;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "city")
    private String city;

    public Patient(String deviceId, String room, String gender, String name, String street, String zip, String city) {
        this.deviceId = deviceId;
        this.room = room;
        this.gender = gender;
        this.name = name;
        this.street = street;
        this.zip = zip;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", room='" + room + '\'' +
                ", gender='" + gender + '\'' +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", zip='" + zip + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
