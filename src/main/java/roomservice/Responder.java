package roomservice;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.SpeechletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class where Alexa request-response handled/generated (main flow)
 */
class Responder {

    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static final Logger log = LoggerFactory.getLogger(Responder.class);
    private Speaker speaker = new Speaker();
    private RequestFileRepository requestFileRepository = new RequestFileRepository();
    private MealsRepository mealsRepository = new MealsRepository();

    /**
     * Handles request-response from Alexa
     *
     * @param requestEnvelope
     * @param intent
     * @param session
     * @return
     */
    SpeechletResponse respondToIntent(SpeechletRequestEnvelope requestEnvelope, Intent intent, UserSession session) {
        try {
            String intentName = (intent != null) ? intent.getName() : null;
            Patient patient = getPatientFromSession(session);
            log.info("intent: " + intentName);
            log.info("patient: " + patient);
            log.info("last spoken key -" + session.getLastSpokenCard());

            if ("Emergency".equals(intentName)) {
                processEmergencyRequest(patient);
                return speaker.speakAndFinish("EmergencyDone", session.getGender(), session.getPatientName());

            } else if ("Normal".equals(intentName)) {
                return speaker.repeatedSpeech("HelpOrMeal", session);

            } else if ("Help".equals(intentName)) {
                if (!session.askedFor("HelpOrMeal")) return speaker.repeat(session);
                return speaker.repeatedSpeech("Reason", session, session.getGender(), session.getPatientName());

            } else if ("ContactReason".equals(intentName)) {
                String reason = intent.getSlot("reason").getValue();
                log.info("reason - " + reason);
                if (!session.askedFor("Reason")) return speaker.repeat(session);
                session.setReason(reason);
                processNormalRequest(patient, session.getReason());
                return speaker.repeatedSpeech("NormalDone", session, session.getGender(), session.getPatientName(), session.getReason(), session.getRoom());

            } else if ("AMAZON.YesIntent".equals(intentName)) {
                log.info("yes");
                //2 options
                // - start again
                // - override meal
                if (session.isOverrideMealFlow()) {
                    return speaker.speakMealOptions(session.getMealChoiceTimeOfTheDay(), mealsRepository.getMeals(session.getMealChoiceTimeOfTheDay()), session);
                } else {
                    return speaker.repeatedSpeech("HelpOrMeal", session);
                }

            } else if ("AMAZON.NoIntent".equals(intentName)) {
                log.info("no");
                //2 options
                // - start again
                // - override meal
                if (session.isOverrideMealFlow()) {
                    return speaker.repeatedSpeech("HelpOrMeal", session);
                } else {
                    return speaker.speakAndFinish("Bye", session.getGender(), session.getPatientName());
                }

            } else if ("AMAZON.StopIntent".equals(intentName)) {
                return speaker.speakAndFinish("Bye", session.getGender(), session.getPatientName());

            } else if ("AMAZON.RepeatIntent".equals(intentName)) {
                return speaker.repeatedSpeech(session.getLastSpokenSpeech());

            } else if ("ListenToMealPlanForTomorrow".equals(intentName)) {
                if (!session.askedFor("HelpOrMeal")) return speaker.repeat(session);
                return speaker.repeatedSpeech("WhichMeal", session);

            } else if ("HearMealOptions".equals(intentName)) {
                String mealOfTheDay = intent.getSlot("choice").getValue();
                log.info("choice - " + mealOfTheDay);
                mealOfTheDay = Meal.resolveMeal(mealOfTheDay);
                if (mealOfTheDay == null) return speaker.repeat(session);
                session.setMealChoiceTimeOfTheDay(mealOfTheDay);
                //check if already present
                RequestFile mealRequest = requestFileRepository.isThereAlreadyMealRequest(session.getRoom(), mealOfTheDay);
                if (mealRequest != null) {
                    session.setFlow("OverrideMeal");
                    String menuPresent = mealRequest.getReason().substring(0, mealRequest.getReason().length() - 2) + ": Menü " +
                            mealRequest.getReason().charAt(mealRequest.getReason().length() - 1);
                    return speaker.repeatedSpeech("AlreadyMealPresent", session, mealsRepository.getDay(), mealRequest.getDate(),
                            menuPresent);
                } else {
                    return speaker.speakMealOptions(mealOfTheDay, mealsRepository.getMeals(mealOfTheDay), session);
                }

            } else if ("WhatIsMenuN".equals(intentName)) {
                String no = intent.getSlot("no").getValue();
                log.info("no of the meal to repeat - " + no);
                int noInt = 1;
                try {
                    noInt = Integer.parseInt(no);
                    if (noInt > 3) {
                        return speaker.repeat(session);
                    }
                } catch (NumberFormatException e) {
                    return speaker.repeat(session);
                }
                return speaker.repeatedSpeech("MenuRepeat", session, no,
                        mealsRepository.getMeals(session.getMealChoiceTimeOfTheDay()).get(noInt - 1).getMeal());

            } else if ("MakeMealChoice".equals(intentName)) {
                String no = intent.getSlot("no").getValue();
                log.info("no of the meal choice - " + no);
                session.setMealChoiceNo(no);
                try {
                    if (Integer.parseInt(no) > 3) {
                        return speaker.repeat(session);
                    }
                } catch (NumberFormatException e) {
                    return speaker.repeat(session);
                }
                processMealRequest(session.getMealChoiceTimeOfTheDay(), session.getMealChoiceNo(), patient);
                session.setFlow("");
                return speaker.repeatedSpeech("MealChoiceDone", session, session.getGender(), session.getPatientName(), session.getMealChoiceNo(), session.getMealChoiceTimeOfTheDay());
            }

            return speaker.repeat(session);
        } catch (Exception e) {
            log.error("error", e);
            return speaker.repeatedSpeech("Error", session);
        }
    }

    private Patient getPatientFromSession(UserSession session) {
        Patient patient = new Patient();
        patient.setCity(session.getCity());
        patient.setName(session.getPatientName());
        patient.setGender(session.getGender());
        patient.setRoom(session.getRoom());
        patient.setStreet(session.getStreet());
        patient.setZip(session.getZip());
        return patient;
    }

    private void processMealRequest(String mealChoiceTimeOfTheDay, String mealChoiceNo, Patient patient/*, boolean override*/) throws Exception {
        String reason = mealChoiceTimeOfTheDay + " " + mealChoiceNo;
        fileRequest(patient, reason, reason + " im Raum ");
    }

    private void processNormalRequest(Patient patient, String reason) throws Exception {
        fileRequest(patient, reason, reason + " im Raum ");
    }

    private void processEmergencyRequest(Patient patient) throws Exception {
        fileRequest(patient, "Notfall", "Notfall im Raum ");
    }

    private void fileRequest(Patient patient, String reason, String subj) throws MessagingException, UnsupportedEncodingException {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        LocalTime localTime = zonedDateTime.toLocalTime();
        String time = localTime.format(TIME_FORMATTER);
        requestFileRepository.createRequest(patient.getRoom(), reason, time, LocalDate.now().format(FORMATTER));
        //send email with new request
        SESSender.send(subj + patient.getRoom() + " um " + time, patient);
    }
}
