package roomservice;

import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SsmlOutputSpeech;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class where the output speech is prepared
 */
public class Speaker {

    public static final String I_DIDN_T_CATCH_BEGINNING = "Ich konnte Sie leider nicht verstehen";
    private static final Logger log = LoggerFactory.getLogger(Speaker.class);
    private static ResourceBundle messages = ResourceBundle.getBundle("messages", Locale.GERMANY);

    public static String getMessage(String cardName) {
        return messages.getString(cardName);
    }

    public static String getMessage(String key, String... args) {
        return MessageFormat.format(messages.getString(key), args);
    }

    protected SpeechletResponse repeat(UserSession session) {
        String key = session.getLastSpokenCard();
        String lastSaid = session.getLastSpokenSpeech();
        String speech = "";
        if (lastSaid.contains(I_DIDN_T_CATCH_BEGINNING)) {
            log.info("contains i didnt catch");
            speech = lastSaid;
        } else {
            log.info("doesnt contain i didnt catch");
            speech = getMessage("DidntCatch", session.getGender(), session.getPatientName()) + " " + lastSaid;
        }
        if (key.equalsIgnoreCase("Welcome")) speech = getMessage("DidntCatchWelcome", session.getGender(), session.getPatientName());
        if (key.equalsIgnoreCase("HelpOrMeal")) speech = getMessage("DidntCatchHelpOrMeal");
        if (key.equalsIgnoreCase("MealOptions")) speech = getMessage("DidntCatchMealChoice", session.getGender(), session.getPatientName());
        if (key.equalsIgnoreCase("Reason")) speech = getMessage("DidntCatchReason");
        session.setLastSpokenCard(key);
        session.setLastSpokenSpeech(speech);
        return speak(speech, false);
    }


    protected SpeechletResponse repeatedSpeechWelcome(UserSession session) {
        session.setLastSpokenCard("Welcome");
        String string = MessageFormat.format(messages.getString("Welcome"), session.getGender(), session.getPatientName());
        session.setLastSpokenSpeech(string);
        return speak(string, false);
    }

    protected SpeechletResponse repeatedSpeech(String key, UserSession session) {
        session.setLastSpokenCard(key);
        String string = messages.getString(key);
        session.setLastSpokenSpeech(string);
        return speak(string, false);
    }

    protected SpeechletResponse repeatedSpeech(String speech) {
        return speak(speech, false);
    }

    protected SpeechletResponse repeatedSpeech(String key, UserSession session, String... args) {
        session.setLastSpokenCard(key);
        String string = MessageFormat.format(messages.getString(key), args);
        session.setLastSpokenSpeech(string);
        return speak(string, false);
    }

    /**
     * Ends the skill
     * @param key
     * @param args
     * @return
     */
    protected SpeechletResponse speakAndFinish(String key, String... args) {
        return speak(MessageFormat.format(messages.getString(key), args), true);
    }

    private SpeechletResponse speak(String speechText, boolean end) {
        SsmlOutputSpeech speech = new SsmlOutputSpeech();
        speech.setSsml("<speak>" + speechText + "</speak>");
        log.info("return speech " + speechText);

        if (end) {
            return SpeechletResponse.newTellResponse(speech);
        } else {
            Reprompt reprompt = new Reprompt();
            reprompt.setOutputSpeech(speech);
            return SpeechletResponse.newAskResponse(speech, reprompt);
        }
    }

    /**
     * Meal options speech
     *
     * @param mealOfTheDay
     * @param meals
     * @param session
     * @return
     */
    public SpeechletResponse speakMealOptions(String mealOfTheDay, List<Meal> meals, UserSession session) {
        log.info("meal size - " + meals.size());
        String mealsSpeech = "";
        String day = meals.get(0).getDay();
        String date = meals.get(0).getDate();
        for (Meal meal : meals) {
            mealsSpeech += "Menü";
            mealsSpeech += " ";
            mealsSpeech += meal.getNo();
            mealsSpeech += ": ";
            mealsSpeech += meal.getMeal();
            mealsSpeech += ". ";
        }
        log.info("meal speech - " + mealsSpeech);
        String mealOptions = MessageFormat.format(messages.getString("MealOptions"), day, date, mealOfTheDay, mealsSpeech, session.getGender(), session.getPatientName());
        log.info("meal options - " + mealOptions);
        session.setLastSpokenCard("MealOptions");
        session.setLastSpokenSpeech(mealOptions);
        return speak(mealOptions, false);
    }
}
