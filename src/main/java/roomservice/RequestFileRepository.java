package roomservice;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RequestFileRepository {
    //DynamoDB credentials for IAM role "Pflegerruf"
    private static BasicAWSCredentials creds = new BasicAWSCredentials("AKIAIOF5B26KQEQCCUTQ", "pY33bUBHxuOXc93Phlcf5OHqswxJmU4k0d+81GTx");//client
    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(Regions.US_EAST_1).build();
    static private DynamoDBMapper mapper = new DynamoDBMapper(client);

    /**
     * Adds a new value to RequestFile table
     *
     * @param room
     * @param reason
     * @param time
     * @param date
     * @return
     */
    RequestFile createRequest(String room, String reason, String time, String date/*, boolean override*/) {
        RequestFile requestFile = new RequestFile();
        requestFile.setDate(date);
        requestFile.setTime(time);
        requestFile.setReason(reason);
        requestFile.setStatus("offen");
        requestFile.setId(generateId(requestFile));
        requestFile.setRoom(room);
        mapper.save(requestFile);
        return requestFile;
    }

    private String generateId(Object object) {
        return String.valueOf(System.currentTimeMillis()) + String.valueOf(object.hashCode());
    }

    /**
     * Checks if there is already a meal chosen
     *
     * @param room
     * @param mealOfTheDay
     * @return
     */
    public RequestFile isThereAlreadyMealRequest(String room, String mealOfTheDay) {
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":val1", new AttributeValue().withS(room));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("room = :val1").withExpressionAttributeValues(eav);
        List<RequestFile> scanResult = mapper.scan(RequestFile.class, scanExpression);
        for (RequestFile requestFile : scanResult) {
            if (requestFile.getReason().toLowerCase().startsWith(mealOfTheDay.toLowerCase())) {
                return requestFile;
            }
        }
        return null;
    }
}
