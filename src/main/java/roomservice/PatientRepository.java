package roomservice;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PatientRepository {

    /**
     * DynamoDB credentials for IAM role "Pflegeruf"
     */
    private static BasicAWSCredentials creds = new BasicAWSCredentials(" AKIAIOF5B26KQEQCCUTQ", "pY33bUBHxuOXc93Phlcf5OHqswxJmU4k0d+81GTx");//client
    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(Regions.US_EAST_1).build();
    static private DynamoDBMapper mapper = new DynamoDBMapper(client);

    private Logger logger = LoggerFactory.getLogger(PatientRepository.class);

    /**
     * Populate Patients into DynamoDB
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        PatientRepository repository = new PatientRepository();
        List<Patient> devices = new ArrayList<>();
        devices.add(new Patient("-", "1", "Herr", "Mike Müller", "Müllerstraße 52", "55555", "Berlin"));
        devices.add(new Patient("-", "2", "Frau", "Heike Müller", "Müllerstraße 100", "55577", "München"));
        devices.add(new Patient("-", "100", "Herr", "Marko Müller", "Müllerstraße 300", "58877", "Hamburg"));
        devices.add(new Patient("-", "200", "Frau", "Martina Müller", "Müllerstraße 2", "444", "Stuttgart"));
        for (Patient device : devices) {
            device.setId(String.valueOf(System.currentTimeMillis()) + String.valueOf(device.hashCode()));
            mapper.save(device);
        }
        System.out.println(repository.getPatient("sdfafgdsf"));
    }

    /**
     * Gets a patient details by the deviceID
     *
     * @param deviceId
     * @return
     */
    public Patient getPatient(String deviceId) {
        logger.info("Trying to get patients...");
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":val1", new AttributeValue().withS(deviceId));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("deviceId = :val1").withExpressionAttributeValues(eav);
        List<Patient> scanResult = mapper.scan(Patient.class, scanExpression);
        if (!scanResult.isEmpty()) {
            return scanResult.get(0);
        } else {
            return new Patient("-", "Empfang", "Unbekannt", "Nutzer", "Musterstraße 3", "44444", "Musterstadt");
        }
    }
}
