package roomservice;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.Getter;
import lombok.Setter;

/**
 * Patient RequestFile entity
 */
@DynamoDBTable(tableName = "Pflegerruf-RequestFile")
public class RequestFile {

    @DynamoDBHashKey(attributeName = "id")
    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "reason")
    private String reason;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "room")
    private String room;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "status")
    private String status;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "time")
    private String time;
    @Getter
    @Setter
    @DynamoDBAttribute(attributeName = "date")
    private String date;
}
