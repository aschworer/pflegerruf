package roomservice;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static roomservice.Responder.FORMATTER;
import static roomservice.Responder.TIME_FORMATTER;


public class MealsRepository {

    //DynamoDB credentials for IAM role "Pflegeruf"
    private static BasicAWSCredentials creds = new BasicAWSCredentials(" AKIAIOF5B26KQEQCCUTQ", "pY33bUBHxuOXc93Phlcf5OHqswxJmU4k0d+81GTx");//client
    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(Regions.US_EAST_1).build();
    static private DynamoDBMapper mapper = new DynamoDBMapper(client);

    private Logger logger = LoggerFactory.getLogger(MealsRepository.class);

    /**
     * Getting the All meals from DynamoDB "Meals" table
     *
     * @param timeOfTheDay
     * @return
     */
    public List<Meal> getMeals(String timeOfTheDay) {
        logger.info("Trying to get meals...");
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":val1", new AttributeValue().withS(timeOfTheDay.substring(0, 1).toUpperCase() + timeOfTheDay.substring(1, timeOfTheDay.length())));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("timeOfTheDay = :val1").withExpressionAttributeValues(eav);
        List<Meal> scanResult = mapper.scan(Meal.class, scanExpression);
        ArrayList<Meal> mealsNormalList = new ArrayList<>(scanResult);
        mealsNormalList.sort(Comparator.comparing(Meal::getNo));
        return mealsNormalList;
    }

    /**
     * Gets the day from where the menus are
     * @return
     */
    public String getDay() {
        Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":val1", new AttributeValue().withS("Frühstück"));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("timeOfTheDay = :val1").withExpressionAttributeValues(eav);
        List<Meal> scanResult = mapper.scan(Meal.class, scanExpression);
        return scanResult.get(0).getDay();
    }

    /**
     * Populate menu into DynamoDb
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        LocalTime localTime = zonedDateTime.toLocalTime();
        String time = localTime.format(TIME_FORMATTER);
        MealsRepository repository = new MealsRepository();
        List<Meal> meals = new ArrayList<>();
        meals.add(new Meal("Frühstück", "1", "Rührerei mit Speck und Brot", "Freitag", LocalDate.now().format(FORMATTER)));
        meals.add(new Meal("Frühstück", "2", "Joghurt mit Früchten, Nüssen und Honig", "Freitag", LocalDate.now().format(FORMATTER)));
        meals.add(new Meal("Frühstück", "3", "Brot mit Käse und Rohkost", "Freitag", LocalDate.now().format(FORMATTER)));

        meals.add(new Meal("Mittagessen", "1", "Currywurst mit Pommes", "Freitag", LocalDate.now().format(FORMATTER)));
        meals.add(new Meal("Mittagessen", "2", "Linsensuppe mit Speck", "Freitag", LocalDate.now().format(FORMATTER)));
        meals.add(new Meal("Mittagessen", "3", "Käsespätzle mit Röstzwiebeln", "Freitag", LocalDate.now().format(FORMATTER)));

        meals.add(new Meal("Abendessen", "1", "Brotzeit mit Aufschnitt und Butter", "Freitag", LocalDate.now().format(FORMATTER)));
        meals.add(new Meal("Abendessen", "2", "Brotzeit mit Käse", "Freitag", LocalDate.now().format(FORMATTER)));
        meals.add(new Meal("Abendessen", "3", "Joghurt mit Früchten", "Freitag", LocalDate.now().format(FORMATTER)));

        for (Meal meal : meals) {
            meal.setId(String.valueOf(System.currentTimeMillis()) + String.valueOf(meal.hashCode()));
            mapper.save(meal);
        }
        System.out.println(repository.getMeals("frühstück").size());
    }

}
